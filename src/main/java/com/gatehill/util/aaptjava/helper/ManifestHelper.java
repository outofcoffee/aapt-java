package com.gatehill.util.aaptjava.helper;

import com.gatehill.util.aaptjava.Parser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author pete
 */
public class ManifestHelper {
    private final XPath xPath = XPathFactory.newInstance().newXPath();
    private final Document document;

    private ManifestHelper(Document document) {
        this.document = document;
    }

    public synchronized static ManifestHelper getInstance(Path binaryManifestFile) throws
            IOException, ParserConfigurationException, SAXException {

        final String xml = Parser.decompressXML(Files.readAllBytes(binaryManifestFile));

        final DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        final Document document = documentBuilder.parse(new ByteArrayInputStream(xml.getBytes()));

        return new ManifestHelper(document);
    }

    public String getAppPackage() throws XPathExpressionException {
        return ((Node) xPath.evaluate("/manifest/@package", document.getDocumentElement(), XPathConstants.NODE)).getNodeValue();
    }

    public String getAppVersionName() throws XPathExpressionException {
        return ((Node) xPath.evaluate("/manifest/@versionName", document.getDocumentElement(), XPathConstants.NODE)).getNodeValue();
    }
}
