package com.gatehill.util.aaptjava.cmdline;

import com.gatehill.util.aaptjava.Parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            throw new RuntimeException("Specify a filename.");
        }

        final String xml = Parser.decompressXML(Files.readAllBytes(Paths.get(args[0])));
        System.out.println(xml);
    }
}
