package com.gatehill.util.aaptjava.helper;

import com.gatehill.util.aaptjava.helper.ManifestHelper;
import com.gatehill.util.aaptjava.support.TestUtil;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author pete
 */
public class ManifestHelperTest {
    private ManifestHelper helper;

    @Before
    public void setUp() throws Exception {
        helper = ManifestHelper.getInstance(TestUtil.getBinaryManifestPath());
    }

    @Test
    public void testGetAppPackage() throws Exception {
        final String appPackage = helper.getAppPackage();
        assertEquals("eu.foxse.space", appPackage);
    }

    @Test
    public void testGetAppVersionName() throws Exception {
        final String versionName = helper.getAppVersionName();
        assertEquals("1.0", versionName);
    }
}