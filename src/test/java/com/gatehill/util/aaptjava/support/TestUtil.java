package com.gatehill.util.aaptjava.support;

import org.xmlunit.diff.Diff;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author pete
 */
public class TestUtil {
    public static Path getBinaryManifestPath() throws URISyntaxException {
        return Paths.get(TestUtil.class.getResource("/binary/AndroidManifest.xml").toURI());
    }

    public static Path getTextManifestPath() throws URISyntaxException {
        return Paths.get(TestUtil.class.getResource("/text/AndroidManifest.xml").toURI());
    }

    public static String getDifferencesAsString(Diff d) {
        final StringBuilder sb = new StringBuilder();
        d.getDifferences().forEach(diff -> sb.append(diff.toString()));
        return sb.toString();
    }
}
