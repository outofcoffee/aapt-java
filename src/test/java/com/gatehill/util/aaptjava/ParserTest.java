package com.gatehill.util.aaptjava;

import com.gatehill.util.aaptjava.support.TestUtil;
import org.junit.Test;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.Diff;

import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;

/**
 * @author pete
 */
public class ParserTest {

    @Test
    public void testDecompressXML() throws Exception {
        // test data
        Path input = TestUtil.getBinaryManifestPath();

        // test
        String output = Parser.decompressXML(Files.readAllBytes(input));

        // verify output
        assertNotNull(output);

        Diff d = DiffBuilder.compare(Input.fromFile(TestUtil.getTextManifestPath().toString()))
                .withTest(output).build();

        assertFalse(TestUtil.getDifferencesAsString(d), d.hasDifferences());
    }
}