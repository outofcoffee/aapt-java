package com.gatehill.util.aaptjava.cmdline;

import com.gatehill.util.aaptjava.cmdline.Main;
import com.gatehill.util.aaptjava.support.TestUtil;
import org.junit.Test;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.Diff;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Path;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

/**
 * @author pete
 */
public class MainTest {

    @Test
    public void testMain() throws Exception {
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            // capture output
            System.setOut(new PrintStream(output));

            // test data
            Path input = TestUtil.getBinaryManifestPath();

            // test
            Main.main(new String[]{input.toString()});

            // verify output
            assertNotNull(output);

            Diff d = DiffBuilder.compare(Input.fromFile(TestUtil.getTextManifestPath().toString()))
                    .withTest(output.toByteArray()).build();

            assertFalse(TestUtil.getDifferencesAsString(d), d.hasDifferences());
        }
    }
}