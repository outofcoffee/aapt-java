# aapt-java

A simple library for parsing Android aapt binary AndroidManifest.xml files.

## Examples

Initialise:

    Path binaryManifestPath = Paths.get("/path/to/myapp.apk");
    ManifestHelper helper = ManifestHelper.getInstance(binaryManifestPath);

Read the app package name:

    // read the app package
    final String appPackage = helper.getAppPackage();
    assertEquals("com.example.MyApp", appPackage);

Read the app version name:

    // read the app version name
    final String versionName = helper.getAppVersionName();
    assertEquals("1.0", versionName);

# Requirements

Java 8+

# Use it in your project

## Gradle

Add the following Gradle repository and dependency:

    repositories {
        maven {
            url 'https://dl.bintray.com/outofcoffee/maven'
        }
    }
    
    // ...
    
    dependencies {
        compile 'com.gatehill.util:aapt-java:0.1'
    }

## sbt

Add the following to your ``build.sbt`` file:

Add repo:

    resolvers += "aapt-java repo" at "https://dl.bintray.com/outofcoffee/maven"

Add library dependency:

    "com.gatehill.util" % "aapt-java" % "0.1"

# Background

Based on the aapt XML deflation code from AOSP.

# Publishing

The bintray key should be set as an environment variable.

    export BINTRAY_KEY="..."
    gradle bintrayUpload
