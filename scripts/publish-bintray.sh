#!/usr/bin/env bash

set -e

LIB_VERSION="0.1"
LIB_FILE="aapt-java-${LIB_VERSION}.jar"
BINTRAY_PROJECT="appt-java"

if [[ "" == "${BINTRAY_KEY}" ]]; then
    echo "BINTRAY_KEY not set"
    exit 1
fi

echo ""
echo "Publishing version ${LIB_VERSION} to bintray..."

curl -T ../build/libs/${LIB_FILE} \
    -uoutofcoffee:${BINTRAY_KEY} \
    https://api.bintray.com/content/outofcoffee/maven/${BINTRAY_PROJECT}/${LIB_VERSION}/${LIB_FILE}

echo ""
echo "Published version ${LIB_VERSION} to bintray"
